// import { HeroComponent } from "./hero.component";

// describe('HeroComponent', () => {
//     let component: HeroComponent; 

//     beforeEach(() => {
//         component = new HeroComponent();
//     });

//     it('should have the correct hero', () => {
//         component.hero = { id: 1, name: 'SuperDude', strength: 3 };
//         expect(component.hero.name).toEqual('SuperDude');
//     });
// });

// ----------------------------------- TestBed

import { NO_ERRORS_SCHEMA } from "@angular/core";
import { HeroComponent } from "./hero.component";
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from "@angular/platform-browser";

describe('HeroComponent', () => {
    let fixture: ComponentFixture<HeroComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [HeroComponent],
            schemas: [NO_ERRORS_SCHEMA]
        });
        fixture = TestBed.createComponent(HeroComponent);
    });

    it('should have the correct hero', () => {
        fixture.componentInstance.hero = { id: 1, name: 'SuperDude', strength: 3 };
        expect(fixture.componentInstance.hero.name).toEqual('SuperDude');
    });

    it('should render the hero name in an anchor tag', () => {
        // fixture.componentInstance.hero = { id: 1, name: 'SuperDude', strength: 3 };
        // fixture.detectChanges();
        // expect(fixture.nativeElement.querySelector('a').textContent).toContain('SuperDude');

        fixture.componentInstance.hero = { id: 1, name: 'SuperDude', strength: 3 };
        fixture.detectChanges();
        let deA = fixture.debugElement.query(By.css('a'));
        expect(deA.nativeElement.textContent).toContain('SuperDude');
    });

    it('should call onDeleteClick when the delete button is clicked', () => {
        spyOn(fixture.componentInstance, 'onDeleteClick');
        fixture.componentInstance.hero = { id: 1, name: 'SuperDude', strength: 3 };
        fixture.detectChanges();

        let deBtn = fixture.debugElement.query(By.css('button.delete'));
        deBtn.triggerEventHandler('click', { stopPropagation: () => { } });

        expect(fixture.componentInstance.onDeleteClick).toHaveBeenCalled();
    });

    it('should emit delete event when the delete button is clicked', () => {
        spyOn(fixture.componentInstance.delete, 'next');
        fixture.componentInstance.hero = { id: 1, name: 'SuperDude', strength: 3 };
        fixture.detectChanges();

        let deBtn = fixture.debugElement.query(By.css('button.delete'));
        deBtn.triggerEventHandler('click', { stopPropagation: () => { } });

        expect(fixture.componentInstance.delete.next).toHaveBeenCalled();
    });
});