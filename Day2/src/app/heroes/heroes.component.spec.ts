import { of } from "rxjs";
import { Hero } from "../hero";
import { HeroService } from "../hero.service";
import { HeroesComponent } from "./heroes.component";

describe('HeroesComponent', () => {
    let component: HeroesComponent;
    let mockHeroService: jasmine.SpyObj<HeroService>;
    let HEROES: Hero[];

    beforeEach(() => {
        HEROES = [
            { id: 1, name: 'SpiderDude', strength: 8 },
            { id: 2, name: 'Wonderful Womer', strength: 24 },
            { id: 3, name: 'SuperDude', strength: 55 }
        ];

        mockHeroService = jasmine.createSpyObj(['getHeroes', 'addHero', 'deleteHero']);

        component = new HeroesComponent(mockHeroService);
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });

    describe('ngOnInit', ()=>{
        it('should call getHeroes', () => {
            spyOn(component, 'getHeroes');
            component.ngOnInit();
            expect(component.getHeroes).toHaveBeenCalled();
        });
    });

    describe('getHeroes', ()=>{
        it('should set heroes correctly from the service', () => {
            mockHeroService.getHeroes.and.returnValue(of(HEROES));
            component.getHeroes();
            expect(mockHeroService.getHeroes).toHaveBeenCalled();
            // expect(component.heroes).toEqual(HEROES);
        });
    });

    describe('delete', () => {
        it('should remove the indicated hero from the heroes list', () => {
            mockHeroService.deleteHero.and.returnValue(of());
            component.heroes = HEROES;

            component.delete(HEROES[2]);
            expect(component.heroes.length).toBe(2);
        });

        it('should call deleteHero from service', () => {
            mockHeroService.deleteHero.and.returnValue(of());
            component.heroes = HEROES;

            component.delete(HEROES[2]);
            expect(mockHeroService.deleteHero).toHaveBeenCalledWith(HEROES[2]);
        });
    });
});