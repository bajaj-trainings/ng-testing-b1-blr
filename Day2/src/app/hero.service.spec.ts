// import { inject, TestBed } from "@angular/core/testing";
// import { HeroService } from "./hero.service";
// import { MessageService } from "./message.service";
// import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

// describe('HeroService', () => {
//     let httpTestingController: HttpTestingController;
//     let mockMessageService: jasmine.SpyObj<MessageService>;
//     let heroService: HeroService;

//     beforeEach(() => {
//         TestBed.configureTestingModule({
//             imports: [
//                 HttpClientTestingModule
//             ],
//             providers: [
//                 HeroService,
//                 { provide: MessageService, useValue: mockMessageService }
//             ]
//         });

//         // httpTestingController = TestBed.inject(HttpTestingController);
//         // heroService = TestBed.inject(HeroService);
//     });

//     describe('getHero', () => {
//         it('should call get with the correct URL', inject([HeroService, HttpTestingController],
//             (heroService: HeroService, httpTestingController: HttpTestingController) => {
//                 // Call the service getHero method
//                 // test that the correct URL is used
//             })
//         );
//     });
// });

import { inject, TestBed } from "@angular/core/testing";
import { HeroService } from "./hero.service";
import { MessageService } from "./message.service";
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('HeroService', () => {
    let httpTestingController: HttpTestingController;
    let mockMessageService: jasmine.SpyObj<MessageService>;
    let heroService: HeroService;

    beforeEach(() => {
        mockMessageService = jasmine.createSpyObj(['add']);

        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                HeroService,
                { provide: MessageService, useValue: mockMessageService }
            ]
        });

        httpTestingController = TestBed.inject(HttpTestingController);
        heroService = TestBed.inject(HeroService);
    });

    describe('getHero', () => {
        it('should call get with the correct URL', () => {
            heroService.getHero(4).subscribe(hero => {
                expect(hero.id).toEqual(4);
            });
            const req = httpTestingController.expectOne('api/heroes/4');
            req.flush({ id: 4, name: 'SuperDude', strength: 100 });
            httpTestingController.verify();
        });

        it('should use GET verb', () => {
            heroService.getHero(4).subscribe();
            const req = httpTestingController.expectOne('api/heroes/4');
            req.flush({ id: 4, name: 'SuperDude', strength: 100 });
            expect(req.request.method).toEqual('GET');
            httpTestingController.verify();
        });
    });
});