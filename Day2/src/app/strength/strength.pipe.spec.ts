import { StrengthPipe } from "./strength.pipe";

describe('StrengthPipe', () => {
    let pipe: StrengthPipe;

    beforeEach(() => {
        pipe = new StrengthPipe();
    });

    describe('weak', () => {
        it('should display "5 (weak)" if the value is 5', () => {
            expect(pipe.transform(5)).toBe('5 (weak)');
        });

        it('should display "9 (weak)" if the value is 9', () => {
            expect(pipe.transform(9)).toBe('9 (weak)');
        });
    });

    describe('strong', () => {
        it('should display "10 (strong)" if the value is 10', () => {
            expect(pipe.transform(10)).toBe('10 (strong)');
        });

        it('should display "15 (strong)" if the value is 15', () => {
            expect(pipe.transform(15)).toBe('15 (strong)');
        });

        it('should display "19 (strong)" if the value is 19', () => {
            expect(pipe.transform(19)).toBe('19 (strong)');
        });
    });

    describe('unbelievable', () => {
        it('should display "20 (unbelievable)" if the value is 20', () => {
            expect(pipe.transform(20)).toBe('20 (unbelievable)');
        });

        it('should display "25 (unbelievable)" if the value is 25', () => {
            expect(pipe.transform(25)).toBe('25 (unbelievable)');
        });

        it('should display "100 (unbelievable)" if the value is 100', () => {
            expect(pipe.transform(100)).toBe('100 (unbelievable)');
        });
    });
});