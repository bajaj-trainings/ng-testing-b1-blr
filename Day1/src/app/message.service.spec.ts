import { MessageService } from "./message.service";

describe('MessageService', () => {
    let service: MessageService;

    beforeEach(() => {
        service = new MessageService();
    });

    describe('Initial State', () => {
        it('should start with no messages', () => {
            expect(service.messages.length).toBe(0);
        });
    });

    describe('Add Method', () => {
        it('should add a message and update the length', () => {
            service.add('Test Message');
            expect(service.messages.length).toBe(1);
        });
        it('should add a message and store the correct message', () => {
            service.add('Test Message');
            expect(service.messages[0]).toBe('Test Message');
        });
        it('should add multiple messages and update the length', () => {
            service.add('Test Message 1');
            service.add('Test Message 2');
            expect(service.messages.length).toBe(2);
        });
        it('should add multiple messages and store the first message', () => {
            service.add('Test Message 1');
            service.add('Test Message 2');
            expect(service.messages).toContain('Test Message 1');
        });
        it('should add multiple messages and store the second message', () => {
            service.add('Test Message 1');
            service.add('Test Message 2');
            expect(service.messages).toContain('Test Message 2');
        });
    });

    describe('Clear Method', () => {
        it('should clear all messages and update the length', () => {
            service.add('Test Message');
            service.clear();
            expect(service.messages.length).toBe(0);
        });
        it('should clear all messages and result in an empty array', () => {
            service.add('Test message');
            service.clear();
            expect(service.messages).toEqual([]);
        });
    });
});