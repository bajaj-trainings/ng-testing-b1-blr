import { InMemoryDbService } from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      { id: 1, name: 'Superman', strength: 95 },
      { id: 2, name: 'Batman', strength: 70 },
      { id: 3, name: 'Wonder Woman', strength: 90 },
      { id: 4, name: 'Flash', strength: 85 },
      { id: 5, name: 'Green Lantern', strength: 80 },
      { id: 6, name: 'Aquaman', strength: 75 },
      { id: 7, name: 'Cyborg', strength: 65 },
      { id: 8, name: 'Green Arrow', strength: 60 },
      { id: 9, name: 'Martian Manhunter', strength: 88 },
      { id: 10, name: 'Shazam', strength: 92 }
    ];
    
    return {heroes};
  }
}
