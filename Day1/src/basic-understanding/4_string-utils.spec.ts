// import { contains, repeat, toLowerCase, toUpperCase } from "./utilities/string-utils";

// describe("String Utilities", () => {
//     it("should be able to lower case a string", () => {
//         expect(toLowerCase("HELLO")).toEqual("hello");
//     });
//     it("should be able to upper case a string", () => {
//         // expect(toUpperCase("hello")).toEqual("HELLO");

//         // const spytoUpperCase = spyOn(String.prototype, 'toUpperCase');
//         const spytoUpperCase = spyOn(String.prototype, 'toUpperCase').and.callThrough();

//         expect(toUpperCase("hello")).toEqual("HELLO");
//         // expect(String.prototype.toUpperCase).toHaveBeenCalled();
//         expect(spytoUpperCase.calls.count()).toEqual(1);

//     });
//     it("toUppercase() should be called only once", () => {
//         const spytoUpperCase = spyOn(String.prototype, 'toUpperCase').and.callThrough();
//         toUpperCase("hello");
//         expect(spytoUpperCase.calls.count()).toEqual(1);
//     });
//     it("should be able to confirm if a string contains a substring", () => {
//         expect(contains("hello world", "hello", 0)).toBeTruthy();
//     });
//     it("should be able to repeat a string multiple times", () => {
//         expect(repeat("hello", 3)).toEqual("hellohellohello");
//     });
// });