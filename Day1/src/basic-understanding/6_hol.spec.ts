// import { factorial, reverseString, isPalindrome, findMax } from './utilities/hol';

// describe('HOL Functions', () => {
//     describe('factorial', () => {
//         it('should return 1 when given 0', () => {
//             expect(factorial(0)).toBe(1);
//         });
//         it('should return 1 when given 1', () => {
//             expect(factorial(1)).toBe(1);
//         });
//         it('should return the correct factorial for positive numbers', () => {
//             expect(factorial(1)).toBe(1);
//             expect(factorial(2)).toBe(2);
//             expect(factorial(3)).toBe(6);
//             expect(factorial(4)).toBe(24);
//             expect(factorial(5)).toBe(120);
//         });

//         it('should throw an error for negative numbers', () => {
//             expect(() => factorial(-1)).toThrowError('Negative numbers are not allowed.');
//         });
//     });
//     describe('reverseString', () => {
//         it('should return the reversed string', () => {
//             expect(reverseString('hello')).toBe('olleh');
//             expect(reverseString('world')).toBe('dlrow');
//         });

//         it('should handle empty strings', () => {
//             expect(reverseString('')).toBe('');
//         });
//     });
//     describe('isPalindrome', () => {
//         it('should return true for palindromes', () => {
//             expect(isPalindrome('madam')).toBe(true);
//             expect(isPalindrome('racecar')).toBe(true);
//             expect(isPalindrome('A man, a plan, a canal, Panama')).toBe(true);
//         });

//         it('should return false for non-palindromes', () => {
//             expect(isPalindrome('hello')).toBe(false);
//             expect(isPalindrome('world')).toBe(false);
//         });

//         it('should handle empty strings', () => {
//             expect(isPalindrome('')).toBe(true);
//         });
//     });
//     describe('findMax', () => {
//         it('should return the maximum number in an array', () => {
//             expect(findMax([1, 2, 3, 4, 5])).toBe(5);
//             expect(findMax([-1, -2, -3, -4, -5])).toBe(-1);
//         });

//         it('should return null for empty arrays', () => {
//             expect(findMax([])).toBeNull();
//         });
//     });
// });