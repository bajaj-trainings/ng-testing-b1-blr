// import { Calculator } from "./utilities/calculator";

// describe("Calculator", () => {
//     let calculator: Calculator;

//     beforeEach(() => {
//         calculator = new Calculator();
//     });

//     it("should add two number correctly", () => {
//         // const calculator = new Calculator();
//         expect(calculator.add(1, 2)).toEqual(3);
//     })

//     it("should subtract two number correctly", () => {
//         // const calculator = new Calculator();
//         expect(calculator.subtract(5, 2)).toEqual(3);
//     })

//     it('should multiply two numbers correctly', () => {
//         expect(calculator.multiply(2, 3)).toBe(6);
//     });

//     it('should divide two numbers correctly', () => {
//         expect(calculator.divide(6, 3)).toBe(2);
//     });

//     it('should throw an error when dividing by zero', () => {
//         // expect(() => calculator.divide(6, 0)).toThrow();
//         expect(() => calculator.divide(6, 0)).toThrowError('Division by zero');
//     });

//     it('should return true for positive numbers', () => {
//         expect(calculator.isPositive(5)).toBeTrue();
//     });

//     it('should return false for negative numbers', () => {
//         expect(calculator.isPositive(-5)).toBeFalse();
//     });

//     it('should show other matchers', () => {
//         const numbers = [1, 2, 3, 4, 5];
//         expect(numbers).toContain(3);

//         const nullValue = null;
//         const nanValue = NaN;
//         expect(nullValue).toBeNull();
//         expect(nanValue).toBeNaN();

//         expect(calculator.add(2, 3)).toBeGreaterThan(4);
//         expect(calculator.add(2, 3)).toBeLessThan(6);

//         const message = 'Hello Jasmine!';
//         expect(message).toMatch(/jasmine/i);
//     });
// });