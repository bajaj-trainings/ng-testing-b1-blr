// // describe("Math Utilities", () => {
// //     describe("Basic Math Utilities", () => {
// //         it("should be able to tell if number is even", () => {
// //             expect().nothing();
// //         });
// //         it("should be able to tell if number is odd", () => {
// //             expect().nothing();
// //         });
// //     });
// //     describe("Advanced Math Utilities", () => {
// //         it("should be able to tell if number is prime", () => {
// //             expect().nothing();
// //         });
// //         it("should be able to calculate the fibonacci of a number", () => {
// //             expect().nothing();
// //         });
// //     });
// // });

// // -----------------------------------
// import { isEven, isOdd, isPrime, fibonacci } from "./utilities/math-utils";

// describe("Math Utilities", () => {
//     describe("Basic Math Utilities", () => {
//         describe("isEven", () => {
//             it("should return true for even numbers", () => {
//                 // const input = 2;
//                 // const result = isEven(input);
//                 // expect(result).toBe(true); 
//                 expect(isEven(2)).toBe(true);
//                 expect(isEven(4)).toBe(true);
//                 expect(isEven(6)).toBe(true);
//                 expect(isEven(8)).toBe(true);
//             });

//             it("should return false for odd numbers", () => {
//                 expect(isEven(1)).toBe(false);
//                 expect(isEven(3)).toBe(false);
//                 expect(isEven(5)).toBe(false);
//                 expect(isEven(7)).toBe(false);
//             });
//         });

//         describe('isOdd', () => {
//             it('should return true for odd numbers', () => {
//                 expect(isOdd(1)).toBe(true);
//                 expect(isOdd(3)).toBe(true);
//                 expect(isOdd(5)).toBe(true);
//                 expect(isOdd(7)).toBe(true);
//             });

//             it('should return false for even numbers', () => {
//                 expect(isOdd(2)).toBe(false);
//                 expect(isOdd(4)).toBe(false);
//                 expect(isOdd(6)).toBe(false);
//                 expect(isOdd(8)).toBe(false);
//             });
//         });
//     });
//     describe("Advanced Math Utilities", () => {
//         describe('isPrime', () => {
//             it('should return false for numbers less than or equal to 1', () => {
//                 expect(isPrime(0)).toBe(false);
//                 expect(isPrime(1)).toBe(false);
//             });

//             it('should return true for prime numbers', () => {
//                 expect(isPrime(2)).toBe(true);
//                 expect(isPrime(3)).toBe(true);
//                 expect(isPrime(5)).toBe(true);
//                 expect(isPrime(7)).toBe(true);
//             });

//             it('should return false for non-prime numbers', () => {
//                 expect(isPrime(4)).toBe(false);
//                 expect(isPrime(6)).toBe(false);
//                 expect(isPrime(8)).toBe(false);
//                 expect(isPrime(9)).toBe(false);
//             });
//         });

//         describe('fibonacci', () => {
//             it('should return the first two Fibonacci numbers when n is 1', () => {
//                 expect(fibonacci(1)).toEqual([0, 1]);
//             });

//             it('should return the correct Fibonacci sequence for n = 2', () => {
//                 expect(fibonacci(2)).toEqual([0, 1, 1]);
//             });

//             it('should return the correct Fibonacci sequence for n = 3', () => {
//                 expect(fibonacci(3)).toEqual([0, 1, 1, 2]);
//             });

//             it('should return the correct Fibonacci sequence for n = 4', () => {
//                 expect(fibonacci(4)).toEqual([0, 1, 1, 2, 3]);
//             });

//             it('should return the correct Fibonacci sequence for n = 5', () => {
//                 expect(fibonacci(5)).toEqual([0, 1, 1, 2, 3, 5]);
//             });
//         });
//     });
// });
