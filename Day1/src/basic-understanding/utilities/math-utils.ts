export function fibonacci(n: number): number[] {
    if (n === 1) {
        return [0, 1];
    } else {
        const s = fibonacci(n - 1);
        s.push(s[s.length - 1] + s[s.length - 2]);
        return s;
    }
}

export function isPrime(num: number): boolean {
    if (num <= 1) return false;
    for (let i = 2; i < num; i++) {
        if (num % i === 0) return false;
    }
    return true;
}

export function isEven(n: number): boolean {
    return n % 2 === 0;
}

export function isOdd(n: number): boolean {
    return Math.abs(n % 2) === 1;
}

