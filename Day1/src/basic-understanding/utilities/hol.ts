export function factorial(n: number): number {
    if (n < 0) throw new Error("Negative numbers are not allowed.");
    if (n === 0) return 1;
    return n * factorial(n - 1);
}

export function reverseString(str: string): string {
    return str.split('').reverse().join('');
}

export function isPalindrome(str: string): boolean {
    const cleanedStr = str.replace(/[\W_]/g, '').toLowerCase();
    return cleanedStr === cleanedStr.split('').reverse().join('');
}

export function findMax(arr: number[]): number | null {
    if (arr.length === 0) return null;
    return Math.max(...arr);
}
