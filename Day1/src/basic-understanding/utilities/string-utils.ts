export function toLowerCase(str: string): string {
    return str.toLowerCase();
}

export function toUpperCase(str: string): string {
    return str.toUpperCase();
}

export function contains(str: string, substring: string, fromIndex?: number): boolean {
    return str.indexOf(substring, fromIndex) !== -1;
}

export function repeat(str: string, n: number): string {
    return (new Array(n + 1)).join(str);
}