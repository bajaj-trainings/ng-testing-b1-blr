export function fibonacci(n: number): number[] {
    if (n === 1) {
        return [0, 1];
    } else {
        const s = fibonacci(n - 1);
        s.push(s[s.length - 1] + s[s.length - 2]);
        return s;
    }
}

export function isPrime(num: number): boolean {
    if (num <= 1) return false;
    for (let i = 2; i < num; i++) {
        if (num % i === 0) return false;
    }
    return true;
}

export function isEven(n: number): boolean {
    return n % 2 === 0;
}

export function isOdd(n: number): boolean {
    return Math.abs(n % 2) === 1;
}

export function toLowerCase(str: string): string {
    return str.toLowerCase();
}

export function toUpperCase(str: string): string {
    return str.toUpperCase();
}

export function contains(str: string, substring: string, fromIndex?: number): boolean {
    return str.indexOf(substring, fromIndex) !== -1;
}

export function repeat(str: string, n: number): string {
    return (new Array(n + 1)).join(str);
}